﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace System
{
    
    public class RNGCryptoServiceProviderExtensions2 : IDisposable
    {
        const int randomBufferMaxSize = 10000;
        byte[] buffer = new byte[randomBufferMaxSize];
        RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();

        const ulong uintCapacity = (ulong)uint.MaxValue + 1;
        const ushort byteCapacity = 256;

        public void Dispose()
        {
            provider.Dispose();
        }

        public void GetValues(uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, ulong valueRangeSize)
        {
            if (valueRangeSize + valueOffset > uintCapacity)
                throw new Exception("Specified valueRangeSize exceeds the capacity for uint");

            var bitSize = valueRangeSize.BitCount();

            if (bitSize == 1)
                Algorithm_SingleByteAlignedSizes_1(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
            else if (bitSize == 2)
                Algorithm_SingleByteAlignedSizes_2(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
            else if (bitSize == 3)
                Algorithm_SingleByteAlignedSizes_3(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
            else if (bitSize == 4)
                Algorithm_SingleByteAlignedSizes_4(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
            else if (bitSize == 5)
                Algorithm_SingleByteAlignedSizes_2(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
            else if (bitSize == 6)
                Algorithm_SingleByteAlignedSizes_2(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
            else if (bitSize == 7)
                Algorithm_SingleByteAlignedSizes_2(valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, valueRangeSize);
        }

        
        public void Algorithm_SingleByteAlignedSizes_1(uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, ulong valueRangeSize)
        {
            var bufferDemand = valueBufferCount / (8/1);
            if (valueBufferCount % (8/1) != 0) bufferDemand++;
            bufferDemand = Math.Min(bufferDemand, randomBufferMaxSize);

            int valueBufferIndex = 0;
            
            byte inByte;
            byte thisValue;
            while (true)
            {
                provider.GetBytes(buffer, 0, bufferDemand);
                
                for (int bufferIndex = 0; bufferIndex < bufferDemand; bufferIndex++)
                {
                    inByte = buffer[bufferIndex];

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 1);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 1);
                    if (valueBufferIndex == valueBufferCount) return;
                }
            }
        }

        public void Algorithm_SingleByteAlignedSizes_2(uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, ulong valueRangeSize)
        {
            
            var bufferDemand = valueBufferCount / (8 / 2);
            if (valueBufferCount % 4 != 0) bufferDemand++;
            bufferDemand = Math.Min(bufferDemand, randomBufferMaxSize);

            int valueBufferIndex = 0;

            byte inByte;
            byte thisValue;
            while (true)
            {
                provider.GetBytes(buffer, 0, bufferDemand);

                for (int bufferIndex = 0; bufferIndex < bufferDemand; bufferIndex++)
                {
                    inByte = buffer[bufferIndex];

                    thisValue = (byte)(inByte & 2);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 2);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 2);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 2);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 2);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 2);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 2);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 2);
                    if (valueBufferIndex == valueBufferCount) return;
                }
            }
        }

        public void Algorithm_SingleByteAlignedSizes_3(uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, ulong valueRangeSize)
        {
            //
            //10 how many groups of 3 bytes? 8 values per 3 bytes.
            var bufferDemand = (int)Math.Ceiling((decimal)valueBufferCount / (decimal)8);
            bufferDemand *= 3;
            bufferDemand += 1; //Alignment so we can take uint

            int valueBufferIndex = 0;

            byte inByte;
            byte thisValue;
            while (true)
            {
                provider.GetBytes(buffer, 0, bufferDemand);

                for (int bufferIndex = 0; bufferIndex < bufferDemand; bufferIndex += 3)
                {
                    inByte = buffer[bufferIndex];

                    thisValue = (byte)(inByte & 4);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 3);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 4);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 3);
                    if (valueBufferIndex == valueBufferCount) return;

                    //2 bits left in the byte - but 3 are needed

                    thisValue = (byte)(inByte & 4);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 3);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 4);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 3);
                    if (valueBufferIndex == valueBufferCount) return;
                }
            }
        }

        public void Algorithm_SingleByteAlignedSizes_4(uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, ulong valueRangeSize)
        {
            var bufferDemand = valueBufferCount / 2;
            if (valueBufferCount % 2 != 0) bufferDemand++;
            bufferDemand = Math.Min(bufferDemand, randomBufferMaxSize);

            int valueBufferIndex = valueBufferOffset;

            byte inByte;
            byte thisValue;
            while (true)
            {
                provider.GetBytes(buffer, 0, bufferDemand);

                for (int bufferIndex = 0; bufferIndex < bufferDemand; bufferIndex++)
                {
                    inByte = buffer[bufferIndex];

                    thisValue = (byte)(inByte & 8);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 4);
                    if (valueBufferIndex == valueBufferCount) return;

                    thisValue = (byte)(inByte & 8);
                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    inByte = (byte)(inByte >> 4);
                    if (valueBufferIndex == valueBufferCount) return;
                }
            }
        }

        public void Algorithm_SingleByteAlignedSizes_8(uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, ulong valueRangeSize)
        {
            var bufferDemand = valueBufferCount;
            bufferDemand = Math.Min(bufferDemand, randomBufferMaxSize);

            int valueBufferIndex = valueBufferOffset;

            byte thisValue;
            while (true)
            {
                provider.GetBytes(buffer, 0, bufferDemand);

                for (int bufferIndex = 0; bufferIndex < bufferDemand; bufferIndex++)
                {
                    thisValue = buffer[bufferIndex];

                    if (thisValue < valueRangeSize) valueBuffer[valueBufferOffset + valueBufferIndex++] = valueOffset + thisValue;
                    if (valueBufferIndex == valueBufferCount) return;
                }
            }
        }


        public static ulong[] Algorithm_SingleByteAlignedSizes_3(byte[] buffer, int itemCount)
        {
            ulong[] result = new ulong[itemCount];
            var bufferIndex = 0;
            var itemIndex = 0;

            while (true)
            {
                var inByte = buffer[bufferIndex];

                result[itemIndex++] = (byte)(inByte & 8);
                inByte = (byte)(inByte >> 3);

                if (itemIndex == itemCount) return result;

                result[itemIndex++] = (byte)(inByte & 8);
                inByte = (byte)(inByte >> 3);

                if (itemIndex == itemCount) return result;

                result[itemIndex++] = (byte)(inByte & 8);
                inByte = (byte)(inByte >> 3);

                if (itemIndex == itemCount) return result;

                result[itemIndex++] = (byte)(inByte & 8);
                inByte = (byte)(inByte >> 3);

                if (itemIndex == itemCount) return result;
            }
        }

        public static ulong[] Algorithm_SingleByteAlignedSizes_4(byte[] buffer, int itemCount)
        {
            ulong[] result = new ulong[itemCount];
            var bufferIndex = 0;
            var itemIndex = 0;

            while (true)
            {
                var inByte = buffer[bufferIndex];

                result[itemIndex++] = (byte)(inByte & 16);
                inByte = (byte)(inByte >> 4);

                if (itemIndex == itemCount) return result;

                result[itemIndex++] = (byte)(inByte & 16);
                inByte = (byte)(inByte >> 4);

                if (itemIndex == itemCount) return result;
            }
        }

        public static ulong[] Algorithm_SingleByteUnalignedSizes(byte[] buffer, int itemCount, int bitCount)
        {

        }

        //TODO: I'm sure this can be improved in terms of CPU performance (and bandwidth/throughput)
        //Perhaps: instead of using {BitConvertor, ulong, bit shifting, and masking} I could simply use masking to read each bit, then build up the final number directly
        public static ulong[] Algorithm_ByteRange_ToNumber_Shift_Mask2(byte[] buffer, int itemCount, int bitCount)
        {
            if (bitCount == 1)
                return Algorithm_SingleByteAlignedSizes_1(buffer, itemCount);
            if (bitCount == 2)
                return Algorithm_SingleByteAlignedSizes_2(buffer, itemCount);
            if (bitCount == 4)
                return Algorithm_SingleByteAlignedSizes_4(buffer, itemCount);
            else if (bitCount < 8)
                return Algorithm_SingleByteUnalignedSizes(buffer, itemCount, bitCount);

            //var baseByte = (int)Math.Floor((decimal)bufferBitOffset / 8);
            var baseByte = bufferBitOffset / 8;
            var baseBitsIn = bufferBitOffset - (baseByte * 8);
            var trimHigh = 0;

            //Special Single-Byte Scenario: Single-byte Shift-Up Shift-Down
            if (bitCount < 8 && (bitCount - 8 + baseBitsIn) <= 0)
            {
                var byteShift = bufferBitOffset - (baseByte * 8);
                var baseBitsInOffset = byteShift;

                var inByte = buffer[baseByte];

                //Discard excess bits by overflowing with bitshifting
                trimHigh = 8 - bitCount - baseBitsInOffset;
                if (trimHigh > 0)
                {
                    inByte = (byte)(inByte << trimHigh);
                    inByte = (byte)(inByte >> (trimHigh + baseBitsInOffset));
                }
                else
                {
                    inByte = (byte)(inByte >> baseBitsInOffset);
                }

                return (ulong)inByte;
            }

            var baseLastBitCountOut = baseBitsIn + bitCount;

            //Easy: byte boundary aligned - 8, 4, 2, 1
            //But this barely happens - if ever
            if (baseBitsIn == 0 && baseLastBitCountOut == 64)
                return BitConverter.ToUInt64(buffer, baseByte);
            else if (baseBitsIn == 0 && baseLastBitCountOut == 32)
                return BitConverter.ToUInt32(buffer, baseByte);
            else if (baseBitsIn == 0 && baseLastBitCountOut == 16)
                return BitConverter.ToUInt16(buffer, baseByte);
            else if (baseBitsIn == 0 && baseLastBitCountOut == 8)
                return buffer[baseByte];

            //var baseBytesNeeded = (int)Math.Ceiling((decimal)baseLastBitCountOut / 8);
            var baseBytesNeeded = baseLastBitCountOut / 8 + (baseLastBitCountOut % 8 != 0 ? 1 : 0);
            ulong referenceValue = 0;
            //Harder: Not byte boundary aligned
            //TODO: Byte Order considerations
            if (baseBytesNeeded > 8)
                //Hybrid - Fallback to more general algorigm
                return Algorithm_BitCopy_Head_Middle_Tail(buffer, bufferBitOffset, bitCount);

            if (baseBytesNeeded > 4)
                referenceValue = BitConverter.ToUInt64(buffer, baseByte); //ToNumber
            else if (baseBytesNeeded > 2)
                referenceValue = BitConverter.ToUInt32(buffer, baseByte); //ToNumber
            else if (baseBytesNeeded > 1)
                referenceValue = BitConverter.ToUInt16(buffer, baseByte); //ToNumber

            //trimHigh = 64 - bitCount - baseBitsIn;
            //referenceValue = referenceValue << trimHigh;
            //referenceValue = referenceValue >> baseBitsIn + trimHigh;

            referenceValue = referenceValue >> baseBitsIn; //Shift - Align the bits again to the byte
            referenceValue = referenceValue & mask; //Mask - Mask in the bits we're interested in valuing


            return referenceValue;
        }

        
    }
}
