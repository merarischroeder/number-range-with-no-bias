﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    static class WholeNumberExtensions
    {
        public static int BitCount(this uint n)
        {
            int bits = 0;
            if (n > 32767)
            {
                n >>= 16;
                bits += 16;
            }
            if (n > 127)
            {
                n >>= 8;
                bits += 8;
            }
            if (n > 7)
            {
                n >>= 4;
                bits += 4;
            }
            if (n > 1)
            {
                n >>= 2;
                bits += 2;
            }
            if (n > 0)
            {
                bits++;
            }

            return bits;
        }

        

        public static int BitCount(this ulong n)
        {
            int bits = 0;
            if (n > 2147483648) //2^(32-1)-1
            {
                n >>= 32;
                bits += 32;
            }
            if (n > 32767) //2^(16-1)-1
            {
                n >>= 16;
                bits += 16;
            }
            if (n > 127) //2^(8-1)-1
            {
                n >>= 8;
                bits += 8;
            }
            if (n > 7) //2^(4-1)-1
            {
                n >>= 4;
                bits += 4;
            }
            if (n > 1) //2^(2-1)-1
            {
                n >>= 2;
                bits += 2;
            }
            if (n > 0) //2^(1-1)-1
            {
                bits++;
            }

            return bits;
        }

     

        public static int BitCount(this byte n)
        {
            int bits = 0;
            if (n > 127)
            {
                n >>= 8;
                bits += 8;
            }
            if (n > 7)
            {
                n >>= 4;
                bits += 4;
            }
            if (n > 1)
            {
                n >>= 2;
                bits += 2;
            }
            if (n > 0)
            {
                bits++;
            }

            return bits;
        }

       
    }
}
