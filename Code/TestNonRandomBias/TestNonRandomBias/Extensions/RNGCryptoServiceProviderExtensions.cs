﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace System
{
    public static class RNGCryptoServiceProviderExtensions
    {
        const ulong uintCapacity = (ulong)uint.MaxValue + 1;
        const ushort byteCapacity = 256;

        public static void GetValues(this RNGCryptoServiceProvider provider, uint[] valueBuffer, int valueBufferOffset = 0, int valueBufferCount = -1, uint valueOffset = 0, ulong valueRangeSize = uintCapacity)
        {
            //var valueRangeSize = valueRangeSizeParameter ?? (ulong)uint.MaxValue + 1;

            if (valueRangeSize + valueOffset > uintCapacity)
                throw new Exception("Specified valueRangeSize exceeds the capacity for uint");

            if (valueRangeSize == uintCapacity)
                Algorithm_Direct_Uint(provider, valueBuffer, valueBufferOffset, valueBufferCount); //No bias to correct
            else if (valueRangeSize > 256)
                Algorithm_BitStream_Filter(provider, valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, (uint)valueRangeSize);
            else if (valueRangeSize == byteCapacity)
                Algorithm_Direct_ByteToUint(provider, valueBuffer, valueBufferOffset, valueBufferCount, valueOffset); //No bias to correct
            else if (valueRangeSize < 256)
                Algorithm_PerByte_Filter_Modulus(provider, valueBuffer, valueBufferOffset, valueBufferCount, valueOffset, (byte)valueRangeSize);
        }

        static void Algorithm_Direct_ByteToUint(RNGCryptoServiceProvider provider, uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset)
        {
            if (valueOffset + 255 > uint.MaxValue)
                throw new Exception("Random range exceeds the capacity for uint");

            byte[] buffer = new byte[valueBuffer.Length * 1];
            provider.GetBytes(buffer);
            for (int i = 0; i < buffer.Length; i++)
                valueBuffer[valueBufferOffset + i] = valueOffset + buffer[i];
        }

        static void Algorithm_Direct_Uint(RNGCryptoServiceProvider provider, uint[] valueBuffer, int valueBufferOffset, int valueBufferCount)
        {
            byte[] buffer = new byte[valueBuffer.Length * 4];
            provider.GetBytes(buffer);
            Buffer.BlockCopy(buffer, 0, valueBuffer, 0, buffer.Length);
        }

        public static void Algorithm_PerByte_Filter_Modulus(RNGCryptoServiceProvider provider, uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, byte valueRangeSize)
        {
            var maxMultiples = 256 / valueRangeSize; //Finding the byte number boundary above the provided lookup length - the number of bytes
            var exclusiveLimit = (valueRangeSize * maxMultiples); //Expressing that boundary (number of bytes) as an integer

            var resultIndex = 0;

            var buffer = new byte[valueBufferCount];

            while (true)
            {
                var remaining = valueBufferCount - resultIndex;
                if (remaining == 0)
                    break;

                provider.GetBytes(buffer, 0, remaining);

                for (int i = 0; i < remaining; i++) //PerByte
                {
                    if (buffer[i] >= exclusiveLimit) //Filter
                        continue;

                    var value = buffer[i] % valueRangeSize; //Modulus
                    valueBuffer[valueBufferOffset + resultIndex++] = (uint)(valueOffset + value);
                }
            }
        }

        /// <summary>
        /// Removing bias that comes from MOD, any random values in the random stream that exceed the range size are discarded.
        /// Bit manipulation might seem more processor and memory intensive, but this function assumes that very good random byte generation is computationally expensive
        /// </summary>
        /// <param name="values">value buffer to fill</param>
        /// <param name="valueCount">the amount of items in the buffer array to fill</param>
        /// <param name="valueOffset"></param>
        /// <param name="valueRangeSize"></param>
        public static void Algorithm_BitStream_Filter(RNGCryptoServiceProvider provider, uint[] valueBuffer, int valueBufferOffset, int valueBufferCount, uint valueOffset, uint valueRangeSize)
        {
            ulong rangeSize = (ulong)valueRangeSize - (ulong)valueOffset;
            if (valueBufferCount == 0)
                return;
            if (valueBufferCount < 0)
                valueBufferCount = valueBuffer.Length - valueBufferOffset;
            else if (valueBuffer.Length - valueBufferOffset < valueBufferCount)
                throw new Exception("ValueBufferCount is too large");


            //Only get the maximum amount of bits for each value:
            //If we want numbers from 0-100, then 128 is the power-of-2 boundary above, and we need 7 bits for each value.
            var bitCount = rangeSize.BitCount(); // (int)Math.Ceiling(Math.Log(rangeSize) / Math.Log(2));
            //var bitMask = ((ulong)2 << bitCount) - 1; // Math.Pow(2, bitCount) - 1;
            //var minimumRandomBytesNeeded = (int)Math.Ceiling(((decimal)bitCount * valueCount) / 8);

            var buffer = new byte[1024]; //This could be changed to an arbitrary size, or given an absolute upper limit to minimise peak memory usage
            var valueIndex = 0;
            var maximumValueCountPerBuffer = buffer.Length * 8 / bitCount;

            while (true)
            {
                var remaining = valueBufferCount - valueIndex;
                if (remaining == 0)
                    break;

                if (remaining > maximumValueCountPerBuffer) //Limited amount of values that can be processed per full random buffer
                    remaining = maximumValueCountPerBuffer;

                var bitRemaining = (bitCount * remaining);
                var bytesNeeded = bitRemaining / 8 + (bitRemaining % 8 != 0 ? 1 : 0); ;

                provider.GetBytes(buffer, 0, bytesNeeded);

                for (int i = 0; i < remaining; i++)
                {
                    //Get the next value 
                    var bufferBitOffset = i * bitCount;
                    var value = buffer.GetNumberFromBits(bufferBitOffset, bitCount, mask);

                    if (value >= rangeSize)
                        continue;

                    valueBuffer[valueBufferOffset + valueIndex++] = (uint)value + valueOffset;
                }
            }
        }
    }
}
