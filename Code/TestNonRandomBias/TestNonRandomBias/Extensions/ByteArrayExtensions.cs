﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    static class ByteArrayExtensions
    {
        public static ulong GetNumberFromBits(this byte[] buffer, int bufferBitOffset, int bitCount, ulong mask)
        {
            //Benchmarks show is usually faster. Plus it always works beyond 8 bits
            return Algorithm_ByteRange_ToNumber_Shift_Mask(buffer, bufferBitOffset, bitCount, mask);
        }

        /// <summary>
        /// In between the speeds of r1 and r2. But this r3 process is more complex than r1.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="bufferBitOffset"></param>
        /// <param name="bitCount"></param>
        /// <param name="mask"></param>
        /// <returns></returns>
        public static ulong Algorithm_BitCopy_Head_Middle_Tail(byte[] buffer, int bufferBitOffset, int bitCount)
        {
            //Head - First Byte
            ulong value = 0;
            int bitsLeft = bitCount;

            var baseByteIndex = bufferBitOffset / 8;
            var byteCount = 0;
            var byteShift = bufferBitOffset - (baseByteIndex * 8);
            var baseBitsInOffset = byteShift;

            var readMask = (int)0;

            while (baseBitsInOffset < 8 && bitsLeft > 0)
            {
                readMask |= (byte)1 << baseBitsInOffset;
                baseBitsInOffset++;
                bitsLeft--;
            }

            var baseByte = buffer[baseByteIndex];
            var maskedRead = baseByte & readMask;

            //Only one byte? Early escape
            if (byteShift == 0 && bitsLeft == 0)
                return (ulong)maskedRead;
            else if (bitsLeft == 0)
                return (ulong)(maskedRead >> byteShift);

            value = (ulong)(maskedRead >> byteShift);
            baseByteIndex++;
            byteCount++;

            //Middle bytes - quite simple
            while (bitsLeft >= 8)
            {
                baseByte = buffer[baseByteIndex];
                value |= ((ulong)baseByte << (8 * byteCount - byteShift));
                baseByteIndex++;
                byteCount++;
                bitsLeft -= 8;
            }

            if (bitsLeft == 0)
                return value;

            //Last byte
            baseByte = buffer[baseByteIndex];
            readMask = 255 >> (8 - bitsLeft);

            maskedRead = baseByte & readMask;
            value |= ((ulong)maskedRead << (8 * byteCount - byteShift));

            return value;
        }

        /// <summary>
        /// In between the speeds of r1 and r2. But this r3 process is more complex than r1.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="bufferBitOffset"></param>
        /// <param name="bitCount"></param>
        /// <param name="mask"></param>
        /// <returns></returns>
        public static ulong Algorithm_BlockShift_Head_Middle_Tail(byte[] buffer, int bufferBitOffset, int bitCount)
        {
            //Head - First Byte
            

            int bitsLeft = bitCount;

            var baseByteIndex = bufferBitOffset / 8;
            var byteCount = 0;
            var byteShift = bufferBitOffset - (baseByteIndex * 8);
            var baseBitsInOffset = byteShift;

            var inByte = buffer[baseByteIndex];

            //Discard excess bits by overflowing with bitshifting
            var trimHigh = 8 - bitsLeft - baseBitsInOffset;
            if (trimHigh > 0)
            {
                inByte = (byte)(inByte << trimHigh);
                inByte = (byte)(inByte >> (trimHigh + baseBitsInOffset));
            }
            else
            {
                inByte = (byte)(inByte >> baseBitsInOffset);
            }

            //Only one byte to read from? Early escape
            bitsLeft = (bitsLeft - 8 + baseBitsInOffset);
            if (bitsLeft <= 0)
                return (ulong)inByte;

            ulong value = (ulong)inByte;

            value = (ulong)inByte;

            baseByteIndex++;
            byteCount++;

            //Middle bytes - quite simple
            while (bitsLeft >= 8)
            {
                inByte = buffer[baseByteIndex];
                value |= ((ulong)inByte << (8 * byteCount - byteShift));
                baseByteIndex++;
                byteCount++;
                bitsLeft -= 8;
            }

            if (bitsLeft <= 0)
                return value;

            //Last byte
            inByte = buffer[baseByteIndex];
            inByte = (byte)(inByte << (8 - bitsLeft)); //Trim MSB bits
            inByte = (byte)(inByte >> (8 - bitsLeft)); //Back to LSB anchoring
            value |= ((ulong)inByte << (8 * byteCount - byteShift));

            return value;
        }

        //TODO: I'm sure this can be improved in terms of CPU performance (and bandwidth/throughput)
        //Perhaps: instead of using {BitConvertor, ulong, bit shifting, and masking} I could simply use masking to read each bit, then build up the final number directly
        public static ulong Algorithm_ByteRange_ToNumber_Shift_Mask(byte[] buffer, int bufferBitOffset, int bitCount, ulong mask = 0)
        {
            //var baseByte = (int)Math.Floor((decimal)bufferBitOffset / 8);
            var baseByte = bufferBitOffset / 8;
            var baseBitsIn = bufferBitOffset - (baseByte * 8);

            //Special Single-Byte Scenario: Single-byte Shift-Up Shift-Down
            if ((bitCount - 8 + baseBitsIn) <= 0)
            {
                var byteShift = bufferBitOffset - (baseByte * 8);
                var baseBitsInOffset = byteShift;

                var inByte = buffer[baseByte];

                //Discard excess bits by overflowing with bitshifting
                var trimHigh = 8 - bitCount - baseBitsInOffset;
                if (trimHigh > 0)
                {
                    inByte = (byte)(inByte << trimHigh);
                    inByte = (byte)(inByte >> (trimHigh + baseBitsInOffset));
                }
                else
                {
                    inByte = (byte)(inByte >> baseBitsInOffset);
                }

                return (ulong)inByte;
            }

            var baseLastBitCountOut = baseBitsIn + bitCount;

            //Easy: byte boundary aligned - 8, 4, 2, 1
            //But this barely happens - if ever
            //if (baseBitsIn == 0 && baseLastBitCountOut == 64)
            //    return BitConverter.ToUInt64(buffer, baseByte);
            //else if (baseBitsIn == 0 && baseLastBitCountOut == 32)
            //    return BitConverter.ToUInt32(buffer, baseByte);
            //else if (baseBitsIn == 0 && baseLastBitCountOut == 16)
            //    return BitConverter.ToUInt16(buffer, baseByte);
            //else if (baseBitsIn == 0 && baseLastBitCountOut == 8)
            //    return buffer[baseByte];

            //var baseBytesNeeded = (int)Math.Ceiling((decimal)baseLastBitCountOut / 8);
            var baseBytesNeeded = baseLastBitCountOut / 8 + (baseLastBitCountOut % 8 != 0 ? 1 : 0);
            ulong referenceValue = 0;
            //Harder: Not byte boundary aligned
            //TODO: Byte Order considerations
            if (baseBytesNeeded > 8)
                //Hybrid - Fallback to more general algorigm
                return Algorithm_BitCopy_Head_Middle_Tail(buffer, bufferBitOffset, bitCount);

            if (baseBytesNeeded > 4)
                referenceValue = BitConverter.ToUInt64(buffer, baseByte); //ToNumber
            else if (baseBytesNeeded > 2)
                referenceValue = BitConverter.ToUInt32(buffer, baseByte); //ToNumber
            else if (baseBytesNeeded > 1)
                referenceValue = BitConverter.ToUInt16(buffer, baseByte); //ToNumber
            
            referenceValue = referenceValue >> baseBitsIn; //Shift - Align the bits again to the byte
            referenceValue = referenceValue & mask; //Mask - Mask in the bits we're interested in valuing

            return referenceValue;
        }

        
    }
}
