﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace TestNonRandomBias
{
    class AlgorithmBenchmark
    {
        public static void Run()
        {
            using (var provider = new RNGCryptoServiceProvider())
            {
                Console.WriteLine("IsLittleEndian: " + BitConverter.IsLittleEndian);
                Console.WriteLine();
                //SecureRangeOriginal2
                TestAlgorithm("Algorithm_PerByte_Filter_Modulus", ((randomCharacterIndexes, rangeSize) => RNGCryptoServiceProviderExtensions.Algorithm_PerByte_Filter_Modulus(provider, randomCharacterIndexes, 0, randomCharacterIndexes.Length, 0, (byte)rangeSize)));
                Console.WriteLine("");
                TestAlgorithm("Algorithm_BitStream_Filter", (randomCharacterIndexes, rangeSize) => RNGCryptoServiceProviderExtensions.Algorithm_BitStream_Filter(provider, randomCharacterIndexes, 0, randomCharacterIndexes.Length, 0, rangeSize));
            }
        }

        static void TestAlgorithm(string Name, Action<uint[], uint> randomSource, bool smallTest = false)
        {
            var sw = new Stopwatch();
            sw.Start();
            Console.WriteLine("[{0} Algorithm Test Start]", Name);
            TestRandomString(randomSource);

            Console.WriteLine("The following tests count how many of each value is encountered in a set of 10M items. The results are then displayed with the number being counted, then the amount of times encountered, then finally the % deviation from the floor(average).");

            BiasTester(1, randomSource); //6 different values. Values 0-5. Values 6 and 7 are discarded. Requires 3 bit alignment.
            BiasTester(6, randomSource); //6 different values. Values 0-5. Values 6 and 7 are discarded. Requires 3 bit alignment.
            BiasTester(2, randomSource); //2 different values. Values 0-1. No values discarded. Requires 1 bit alignment.
            BiasTester(17, randomSource); //17 different values. Values 0-16. Values 17-31 are discarded. Requires 5 bit alignment.
            sw.Stop();
            Console.WriteLine("[Test End - Duration {0} ms]", sw.ElapsedMilliseconds);
        }

        const string lookup = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        static void TestRandomString(Action<uint[], uint> randomSource)
        {
            Console.WriteLine("A random string of 100 characters:");

            var randomCharacterIndexes = new uint[100];
            randomSource(randomCharacterIndexes, (uint)lookup.Length);
            var sb = new StringBuilder();
            for (int i = 0; i < randomCharacterIndexes.Length; i++)
            {
                sb.Append(lookup[(int)randomCharacterIndexes[i]]);
            }
            Console.WriteLine(sb.ToString());

            Console.WriteLine();
        }


        static void BiasTester(uint rangeSize, Action<uint[], uint> randomSource)
        {
            var bitCount = rangeSize.BitCount(); // (int)Math.Ceiling(Math.Log(rangeSize) / Math.Log(2));
            var bitMax = (ulong)Math.Pow(2, bitCount);

            Console.WriteLine();
            Console.WriteLine("This test: {0} different numbers are randomly selected. From a set of values 0-{1}. Requires each random number be selected from {2} bits in the random bit stream. This means numbers greater than {1} but lower than {3} will be discarded if encountered.",
                rangeSize,
                rangeSize - 1,
                bitCount,
                bitMax);

            var randomCharacterIndexes = new uint[1000 * 1000 * 10]; //~10M items * 4 = ~40MB of memory
            randomSource(randomCharacterIndexes, rangeSize);
            int[] distributionBoxes = new int[rangeSize];
            for (int i = 0; i < randomCharacterIndexes.Length; i++)
            {
                distributionBoxes[randomCharacterIndexes[i]]++;
            }
            //3 bits => 24
            //
            var average = randomCharacterIndexes.Length / rangeSize;
            for (int i = 0; i < distributionBoxes.Length; i++)
            {
                Console.WriteLine("{0}: value count {1} avg. dev. {2:0.00}%", i, distributionBoxes[i], (((decimal)distributionBoxes[i] - average) / average) * 100);
            }
        }

        static void Algorithm_PerByte_Masking(RNGCryptoServiceProvider provider, uint[] result, uint rangeSize)
        {
            //This approach using a bitmask instead of modulus is 2x slower
            if (rangeSize > 256)
            {
                RNGCryptoServiceProviderExtensions.Algorithm_BitStream_Filter(provider, result, 0, result.Length, 0, rangeSize);
                return;
            }

            var bitCount = ((byte)rangeSize).BitCount(); // (int)Math.Ceiling(Math.Log(rangeSize) / Math.Log(2));
            byte bitMask = (byte)((2 << bitCount) - 1);

            var length = result.Length;
            var resultIndex = 0;

            var buffer = new byte[length];

            while (true)
            {
                var remaining = length - resultIndex;
                if (remaining == 0)
                    break;

                provider.GetBytes(buffer, 0, remaining);

                for (int i = 0; i < remaining; i++)
                {
                    var value = buffer[i] & bitMask;

                    if (value >= rangeSize)
                        continue;

                    result[resultIndex++] = (byte)value;
                }
            }

        }

    }
}
