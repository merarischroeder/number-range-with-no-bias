﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TestNonRandomBias
{
    class BitCountBenchmark
    {
        public static void Run()
        {
            var value = (uint)2345;

            var n = (int)Math.Ceiling(Math.Log(value) / Math.Log(2));
            var b = BitCount_Slower(value);
            if (n != b)
            {
                throw new Exception("Unstable algorithm");
            }

            var sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var x = (int)Math.Ceiling(Math.Log(value) / Math.Log(2));
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);


            sw.Restart();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var y = (int)Math.Ceiling(Math.Log(value, 2));
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var z = value.BitCount();
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var z = BitCount_Slower(value);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
        }

        static int BitCount_Slower(uint value)
        {
            int bits = 1;
            for (int b = 16; b >= 1; b /= 2)
            {
                int s = 1 << b;
                if (value >= s) { value >>= b; bits += b; }
            }
            return bits;
        }
    }
}
