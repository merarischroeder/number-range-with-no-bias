﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;

namespace TestNonRandomBias
{
    class BitStreamNumbersBenchmark
    {
        public static void Run()
        {
            byte[] buffer = new byte[1024000];
            using (var provider = new RNGCryptoServiceProvider())
            {
                provider.GetBytes(buffer);
            }

            //Console.WriteLine("BitSize: {0}", bitSize);

            //for (int i = 0; i < cycles; i++)
            //{
            //    var r1 = GetValueFromBits1(buffer, cycles * bitSize, bitSize, mask);
            //    var r2 = GetValueFromBits1(buffer, cycles * bitSize, bitSize, mask);
            //    var r3 = GetValueFromBits1(buffer, cycles * bitSize, bitSize, mask);
            //    var r4 = GetValueFromBits1(buffer, cycles * bitSize, bitSize, mask);

            //    if (r1 != r2)
            //        Console.WriteLine("r1 != r2");
            //    if (r1 != r3)
            //        Console.WriteLine("r1 != r3");
            //    if (r1 != r4)
            //        Console.WriteLine("r1 != r4");
            //}


            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                ulong mask = (x == 64 ? ulong.MaxValue : ((ulong)1 << x) - 1);
                var cycles = buffer.Length / bitSize;
                ulong benchmarkValue = 0;
                ulong compareValue = 0;
                for (int i = 0; i < cycles - 1; i++)
                {
                    benchmarkValue = ByteArrayExtensions.Algorithm_ByteRange_ToNumber_Shift_Mask(buffer, i * bitSize, bitSize, mask);
                    compareValue = ByteArrayExtensions.Algorithm_ByteRange_ToNumber_Shift_Mask2(buffer, i * bitSize, bitSize, mask);
                    if (compareValue != benchmarkValue) throw new Exception("Value mismatch!!");
                    compareValue = Algorithm_MaskReadBitState_SetIfTrue(buffer, i * bitSize, bitSize);
                    if (compareValue != benchmarkValue) throw new Exception("Value mismatch!!");
                    compareValue = ByteArrayExtensions.Algorithm_BitCopy_Head_Middle_Tail(buffer, i * bitSize, bitSize);
                    if (compareValue != benchmarkValue) throw new Exception("Value mismatch!!");
                    compareValue = ByteArrayExtensions.Algorithm_BlockShift_Head_Middle_Tail(buffer, i * bitSize, bitSize);
                    if (compareValue != benchmarkValue) throw new Exception("Value mismatch!!");
                    compareValue = Algorithm_CachedByte_ShiftDownToLSB_ShiftUpToTarget(buffer, i * bitSize, bitSize);
                    if (compareValue != benchmarkValue) throw new Exception("Value mismatch!!");
                }
            }
            Console.WriteLine("Verified consistency.");

            Console.WriteLine("Across all bit sizes 1-64:");
       

            var sw = new Stopwatch();
            sw.Start();

            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                ulong mask = (x == 64 ? ulong.MaxValue : ((ulong)1 << x) - 1);
                var cycles = buffer.Length / bitSize;
                for (int i = 0; i < cycles - 1; i++)
                {
                    ByteArrayExtensions.Algorithm_ByteRange_ToNumber_Shift_Mask(buffer, cycles * bitSize, bitSize, mask);
                }
            }

            sw.Stop();
            Console.WriteLine("Algorithm_ByteRange_ToNumber_Shift_Mask: {0}", sw.ElapsedMilliseconds);

            sw = new Stopwatch();
            sw.Start();

            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                ulong mask = (x == 64 ? ulong.MaxValue : ((ulong)1 << x) - 1);
                var cycles = buffer.Length / bitSize;
                for (int i = 0; i < cycles - 1; i++)
                {
                    ByteArrayExtensions.Algorithm_ByteRange_ToNumber_Shift_Mask2(buffer, cycles * bitSize, bitSize, mask);
                }
            }

            sw.Stop();
            Console.WriteLine("Algorithm_ByteRange_ToNumber_Shift_Mask2: {0}", sw.ElapsedMilliseconds);


            sw.Restart();
            sw.Start();
            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                var cycles = buffer.Length / bitSize;
                for (int i = 0; i < cycles - 1; i++)
                {
                    Algorithm_MaskReadBitState_SetIfTrue(buffer, cycles * bitSize, bitSize);
                }
            }
            sw.Stop();
            Console.WriteLine("Algorithm_MaskReadBitState_SetIfTrue: {0}", sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                var cycles = buffer.Length / bitSize;
                for (int i = 0; i < cycles - 1; i++)
                {
                    ByteArrayExtensions.Algorithm_BitCopy_Head_Middle_Tail(buffer, cycles * bitSize, bitSize);
                }
            }
            sw.Stop();
            Console.WriteLine("Algorithm_BitCopy_Head_Middle_Tail: {0}", sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                var cycles = buffer.Length / bitSize;
                for (int i = 0; i < cycles - 1; i++)
                {
                    ByteArrayExtensions.Algorithm_BlockShift_Head_Middle_Tail(buffer, cycles * bitSize, bitSize);
                }
            }
            sw.Stop();
            Console.WriteLine("Algorithm_BlockShift_Head_Middle_Tail: {0}", sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int x = 1; x < 65; x++)
            {
                var bitSize = x;
                var cycles = buffer.Length / bitSize;
                for (int i = 0; i < cycles - 1; i++)
                {
                    Algorithm_CachedByte_ShiftDownToLSB_ShiftUpToTarget(buffer, cycles * bitSize, bitSize);
                }
            }
            sw.Stop();
            Console.WriteLine("Algorithm_CachedByte_ShiftDownToLSB_ShiftUpToTarget: {0}", sw.ElapsedMilliseconds);
            sw.Restart();

            //var results = new int[64, 6]; //64 bitSizes, 6 algs. to test each time
            var sb = new StringBuilder();
            sb.AppendLine("BitCount, T1, T2, T3, T4, T5, T6");

            for (int x = 1; x < 65; x++)
            {
                Console.WriteLine("");
                var bitSize = x;
                Console.WriteLine("Bit Size: {0}", bitSize);
                ulong mask = (x == 64 ? ulong.MaxValue : ((ulong)1 << x) - 1);
                var cycles = buffer.Length / bitSize;
                sw = new Stopwatch();
                sw.Start();

                for (int z = 0; z < 65; z++) //So it's the same timescale as the aggregate test of all bit sizes
                for (int i = 0; i < cycles - 1; i++)
                {
                    ByteArrayExtensions.Algorithm_ByteRange_ToNumber_Shift_Mask(buffer, cycles * bitSize, bitSize, mask);
                }

                sw.Stop();
                var t1 = sw.ElapsedMilliseconds;
                Console.WriteLine("Algorithm_ByteRange_ToNumber_Shift_Mask: {0}", t1);
                sw.Restart();
                sw.Start();
                for (int z = 0; z < 65; z++) //So it's the same timescale as the aggregate test of all bit sizes
                    for (int i = 0; i < cycles - 1; i++)
                    {
                        ByteArrayExtensions.Algorithm_ByteRange_ToNumber_Shift_Mask2(buffer, cycles * bitSize, bitSize, mask);
                    }

                sw.Stop();
                var t2 = sw.ElapsedMilliseconds;
                Console.WriteLine("Algorithm_ByteRange_ToNumber_Shift_Mask2: {0}", t2);
                sw.Restart();
                sw.Start();
                for (int z = 0; z < 65; z++) //So it's the same timescale as the aggregate test of all bit sizes
                for (int i = 0; i < cycles - 1; i++)
                    {
                        Algorithm_MaskReadBitState_SetIfTrue(buffer, cycles * bitSize, bitSize);
                    }

                sw.Stop();
                var t3 = sw.ElapsedMilliseconds;
                Console.WriteLine("Algorithm_MaskReadBitState_SetIfTrue: {0}", t3);
                sw.Restart();
                sw.Start();
                for (int z = 0; z < 65; z++) //So it's the same timescale as the aggregate test of all bit sizes
                    for (int i = 0; i < cycles - 1; i++)
                    {
                        ByteArrayExtensions.Algorithm_BitCopy_Head_Middle_Tail(buffer, cycles * bitSize, bitSize);
                    }

                sw.Stop();
                var t4 = sw.ElapsedMilliseconds;
                Console.WriteLine("Algorithm_BitCopy_Head_Middle_Tail: {0}", t4);
                sw.Restart();
                sw.Start();
                for (int z = 0; z < 65; z++) //So it's the same timescale as the aggregate test of all bit sizes
                    for (int i = 0; i < cycles - 1; i++)
                    {
                        ByteArrayExtensions.Algorithm_BlockShift_Head_Middle_Tail(buffer, cycles * bitSize, bitSize);
                    }

                sw.Stop();
                var t5 = sw.ElapsedMilliseconds;
                Console.WriteLine("Algorithm_BlockShift_Head_Middle_Tail: {0}", t5);
                sw.Restart();
                sw.Start();
                for (int z = 0; z < 65; z++) //So it's the same timescale as the aggregate test of all bit sizes
                    for (int i = 0; i < cycles - 1; i++)
                    {
                        Algorithm_CachedByte_ShiftDownToLSB_ShiftUpToTarget(buffer, cycles * bitSize, bitSize);
                    }

                sw.Stop();
                var t6 = sw.ElapsedMilliseconds;
                Console.WriteLine("Algorithm_CachedByte_ShiftDownToLSB_ShiftUpToTarget: {0}", t6);
                sw.Restart();

                sb.AppendFormat("{0}, {1}, {2}, {3}, {4}, {5}, {6}\r\n", bitSize, t1, t2,t3,t4,t5,t6);
            }

            Console.WriteLine();
            Console.Write(sb.ToString());
        }


        /// <summary>
        /// This one takes about 4 seconds in full testing compared to 2 seconds for the original and more complex GetValueFromBits
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="bufferBitOffset"></param>
        /// <param name="bitCount"></param>
        /// <param name="mask"></param>
        /// <returns></returns>
        private static ulong Algorithm_MaskReadBitState_SetIfTrue(byte[] buffer, int bufferBitOffset, int bitCount)
        {
            ulong value = 0;
            for (int i = 0; i < bitCount; i++)
            {
                var baseByte = bufferBitOffset / 8;
                var baseBitsIn = bufferBitOffset - (baseByte * 8);
                var ReadMask = 1 << baseBitsIn;
                var bitState = (buffer[baseByte] & ReadMask) == ReadMask;
                bufferBitOffset++;
                if (bitState) value |= ((ulong)1 << i);
            }

            return value;
        }

        //3s
        private static ulong Algorithm_CachedByte_ShiftDownToLSB_ShiftUpToTarget(byte[] buffer, int bufferBitOffset, int bitCount)
        {
            ulong value = 0;
            var baseByte = bufferBitOffset / 8;
            var baseBitsIn = bufferBitOffset - (baseByte * 8);
            var byteValue = (ulong)buffer[baseByte];
            var writeBit = 0;
            while (true)
            {
                value |= ((byteValue >> baseBitsIn) & 1) << writeBit;

                writeBit++;
                if (writeBit == bitCount)
                    break;

                baseBitsIn++;
                if (baseBitsIn == 8)
                {
                    baseBitsIn = 0;
                    baseByte++;
                    byteValue = (ulong)buffer[baseByte];
                }
            }

            return value;
        }
    }
}
