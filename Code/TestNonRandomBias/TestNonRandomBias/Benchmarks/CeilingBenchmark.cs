﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace TestNonRandomBias
{
    class CeilingBenchmark
    {
        public static void Run()
        {
            var sw = new Stopwatch();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var x = (201 / 8) + (201 % 8 != 0 ? 1 : 0);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);


            sw.Restart();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var y = IntCeiling(201, 8);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                var z = MathCeiling(201, 8);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);

            sw.Restart();
            sw.Start();
            for (int i = 0; i < 10 * 1000 * 1000; i++)
            {
                int rem;
                var n = Math.DivRem(201, 8, out rem) + (rem != 0 ? 1 : 0);
                //var n = (201 / 8) + (201 % 8 != 0 ? 1 : 0);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
        }

        static int MathCeiling(int number, int divisor)
        {
            return (int)Math.Ceiling((decimal)number / divisor);
        }

        static int IntCeiling(int number, int divisor)
        {
            return (number / divisor) + (number % divisor != 0 ? 1 : 0);
        }
    }
}
